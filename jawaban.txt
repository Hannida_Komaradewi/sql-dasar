Soal 1 Membuat Database
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 1.

CREATE DATABASE myshop;


Soal 2 Membuat Table di Dalam Database
Buatlah tabel – tabel baru di dalam database myshop sesuai data-data berikut.

users
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, nama varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

categories
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

items
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8) NOT null, FOREIGN KEY(category_id) REFERENCES categories(id) );


Soal 3 Memasukkan Data pada Table
Masukkanlah data data berikut dengan perintah SQL “INSERT INTO . . ” ke dalam table yang sudah dibuat pada soal sebelumnya.

users
INSERT INTO users(nama, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

categories
INSERT INTO categories(name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");
 
items
INSERT INTO items(name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
